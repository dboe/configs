#!/bin/bash
# .bash_aliases file 
# Usually included by proper bashrcs like so:
# 
# if [ -f ~/.bash_aliases ]; then
#     . ~/.bash_aliases
# fi

set -o vi

# Default terminal colouring
export PS1="\[\033[38;5;70m\]\A\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;7m\]\u@\h\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;124m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"
if [ $VIM ]; then
        export PS1='[VIM]\h:\w\$ '
fi

# Default terminal colouring
export PS1="\[\033[38;5;70m\]\A\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;7m\]\u@\h\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;124m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"
if [ $VIM ]; then
        export PS1='[VIM]\h:\w\$ '
fi

#Function to show all apps with extension .x
function extFinder() {
    # show all apps with extension .x
    apropos " .$1 "
}
alias app=extFinder

# use thunderbird to append argument files to new mail
function composeMailWithAttachment() {
    echo "Composing mail with attachments! ... "
    # attach all attributes passed to the function
    str="attachment='"
    for i in "$@"; do 
        new=$(realpath $i)
        echo "Adding path $new"
        str="$str$new,"
    done
    str="${str::-1}'"
    echo $str
    thunderbird -compose $str
}
alias mailAtt=composeMailWithAttachment

# get folder that has been modified last in a given directory
# Example:
#   You have a folder with all your talks in subfolders (~/Talks/Talk1/ and ~/Talks/Talk1/)
#   and want to continue workin on the last Talk you were working on. Then you run
#   
#       $ cd $(actFolder ~/Talks)
# 
#   Of course it can be handy to set an alias for actFolder ~/Talks and other common work places.
#   Just add
#
#       actTalk=$(actFolder $HOME/Talks)
#       export actTalk
#
#   to your .bashrc (after the .bash_aliases import)
function lastModifiedSubfolder() {
    subFolder=$(ls  $1 -t1 | head -n 1)
    fullPath="$1/$subFolder"
    echo $fullPath
}
alias actFolder=lastModifiedSubfolder

# compress and decompress
function tarz() {
        [ "$1" != "" ] && tar -czRf $1.tar.gz $1 && echo "$1.tar.gz created successfully!"|| echo "Usage: tarz [folder_or_file]"
}

function tarj() {
        [ "$1" != "" ] && tar -cjRf $1.tar.bz2 $1 && echo "$1.tar.bz2 created successfully!" || echo "Usage: tarj [folder_or_file]"
}

function utar() {
        [ "$1" != "" ] && tar -xvf $1 || echo "Usage: utar [tar_file_name]"
}

# One command to extract them all
function extract() { 
    if [ -f $1 ] ; then 
      case $1 in 
        *.tar.bz2)   tar xjf $1     ;; 
        *.tar.gz)    tar xzf $1     ;; 
        *.bz2)       bunzip2 $1     ;; 
        *.rar)       unrar e $1     ;; 
        *.gz)        gunzip $1      ;; 
        *.tar)       tar xf $1      ;; 
        *.tbz2)      tar xjf $1     ;; 
        *.tgz)       tar xzf $1     ;; 
        *.zip)       unzip $1       ;; 
        *.Z)         uncompress $1  ;; 
        *.7z)        7z x $1        ;; 
        *)     echo "'$1' cannot be extracted via extract()" ;; 
         esac 
     else 
         echo "'$1' is not a valid file" 
     fi 
}

# Function to show tarfile content
function catTarFile() {
    STR=$1
    filePath=${STR%.tar*}
    tarEx='.tar'
    filePath=$filePath$tarEx
    filePathInTar=${STR#*.tar/*}
    echo "File: "$filePath
    echo "Sub file: "$filePathInTar
    if [ $filePathInTar != "" ] && [ $filePathInTar != "/" ]; then
        nFiles=$(tar --list -f $filePath | grep $filePathInTar* | wc -l)
        echo "Number of Files: "$nFiles
        if [ $nFiles -lt 1 ]; then
            echo "No existing subfolder $filePathInTar"
        elif [ $nFiles -eq 1 ]; then
            tar xf $filePath $filePathInTar -O | cat
        else
            tar --list -f $filePath | grep $filePathInTar*
        fi
    else
        tar --list -f $filePath
    fi
}
alias catar=catTarFile


# inplace compilation of pdf_latex files
function externalize() {
    STR=$1
    # pdfLatexFilePath=`python -c "import osTools; print osTools.resolve($STR)"
    # echo $pdfLatexFilePath
    py_filePath="import sys; import osTools; print osTools.resolve(sys.argv[1])"
    py_fileName="import sys; import os; print os.path.basename(sys.argv[1])"
    py_fileDir="import sys; import os; print os.path.dirname(sys.argv[1])"
    pdfLatexFilePath=`python -c "$py_filePath" $STR`
    pdfLatexFileName=`python -c "$py_fileName" $STR`
    fileDir=`python -c "$py_fileDir" $pdfLatexFilePath`
    subDir=".externalization"
    texFileName="externalizer.tex"
    pdfFileName="externalizer.pdf"
    texFilePath=$fileDir/$texFileName

tex_main=$(cat <<- END
    \\documentclass{standalone}
    \\usepackage{graphicx}
    \\usepackage{amsfonts,amsthm,amsmath,amssymb}
    \\usepackage{siunitx}
    \\usepackage{isotope}
    \\usepackage{xspace}
    \\usepackage[svgnames]{xcolor}
    \\usepackage{bm}
    \\usepackage[super]{nth}
    \\usepackage[T1]{fontenc}
    \\usepackage[utf8]{inputenc}
    \\usepackage{XCharter}
    
    \\begin{document}
    \\input{$pdfLatexFileName}
    \\end{document}
END
)
    cd $fileDir
    # mkdir $subDir
    # cd $subDir
    echo "$tex_main" > "$texFileName"
    pdflatex $texFileName
    mv "$fileDir/$pdfFileName" "${pdfLatexFileName}_ext.pdf"
    convert -density 300 "${pdfLatexFileName}_ext.pdf" -quality 90 "${pdfLatexFileName}_ext.png"
    rm externalizer.*
    cd -
}
alias externalize=externalize

# provide screen with some name for a specific updating process. If you call the same process with the
# same name it will not do anything.
#
# Example:
#   run okular in screen if it is not existing already
#   
#   provideScreen okular
function provideScreen() {
    screenName=$1
    program=$2
    running=`screen -list | grep $screenName | wc -l`
    if [ ${running} -lt 1 ]; then
        screen -S "${screenName}" -d -m "${program}" "${@:3}"
        echo "Screen slot ${screenName}: Program ${program} running with options ${@:3}"
    fi
}
alias provideScreen=provideScreen

# enable color support of ls and grep automatically
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'

# grep for recurseive regular expression enabled search that automatically omits some directories
alias regrep='grep -ER \
    --exclude=*~ \
    --exclude=*.pyc --exclude-dir=__pycache__\
    --exclude=*.np --exclude=*.pack --exclude=*.pdf\
    --exclude-dir=.bzr --exclude-dir=.git --exclude-dir=.svn \
    '

# git aliases
alias incoming='git fetch && git log --graph --boundary --left-right --cherry-pick --decorate HEAD..FETCH_HEAD'
alias outgoing='git fetch && git log --graph --boundary --left-right --cherry-pick --decorate FETCH_HEAD..HEAD'
alias gc='git commit -a'
alias gs='git status --porcelain'

# Add an "alert" alias for long running commands.
#
# Example:
# 
#       $ sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias mkdir='mkdir -pv'  # make missing parent dirs also


# resolve paths
alias resolve='pwd -P'
# pytest falls back in ipdb mode on error
alias ipdbpytest='pytest --pdb --pdbcls=IPython.terminal.debugger:Pdb'
alias ipytest='ipdbpytest'
# python with pdb starting along
alias ipdbpython='python -m ipdb -c "continue"'


# Custom Binaries
if [ -d ~/git/cpip ]; then
	export PATH="$HOME/git/cpip/bin:$PATH"
fi

# Automatic addition of user python libraries to PYTHONPATH if the library is in the ~/git folder
# and the file ~/git/<lib_name>/<lib_name>/__init__.py is present.
# for d in $(find $HOME/git/* -maxdepth 2 -name "__init__.py" -type f -printf '%H\n' | uniq ); do PYTHONPATH="$PYTHONPATH:$d"; done
function getPythonGitModules() {
    BASEDIR=${1:-${HOME}/git/}
    LOCAL_PYTHONPATH=""
    for d in $(find $BASEDIR* -maxdepth 2 -name "__init__.py" -type f -printf '%H\n' | uniq )
    do
        if [ -z "$LOCAL_PYTHONPATH" ]
        then
            LOCAL_PYTHONPATH="$d"
        else
            LOCAL_PYTHONPATH="$LOCAL_PYTHONPATH:$d"
        fi
    done
    echo $LOCAL_PYTHONPATH
}
export PYTHONPATH="${PYTHONPATH:+"$PYTHOPATH:"}:$(getPythonGitModules)"

function writeEnvFile()
{
    BASEDIR=${1:-${HOME}/git/}
    ENVFILE=${2:-${BASEDIR}/.vscode_autogen_env}
    LEAD='### AUTOGENERATED PYTHONPATHS FROM GIT PYTHON MODULES'
    echo "${LEAD}" > $ENVFILE

    # add git modules
    echo "PYTHONPATH=$(getPythonGitModules $BASEDIR):${PYTHONPATH}" >> $ENVFILE
}
# required for vscode
alias vscodeEnv=writeEnvFile

# include other aliases if they are present.
if [ -f ~/.bash_docker_aliases ]; then
    . ~/.bash_docker_aliases
fi

# prepend C include and lib path for gcc with local installs to $HOME
export CPLUS_INDLUE_PATH="$HOME/include:$CPLUS_INDLUE_PATH"
export LIBRARY_PATH="$HOME/lib:$LIBRARY_PATH"

# 2-Feb-2021: Fix for Keyring error with pip. Hopefully new pip will fix it
# soon https://github.com/pypa/pip/issues/7883
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring

# use vlc as screen recorder
function screencapture(){
    vlc screen:// --one-instance -I dummy --extraintf oldrc --rc-host localhost:8082 \
    --no-video :screen-fps=5 :screen-caching=300 \
    --sout "#transcode{vcodec=h264,vb=800,fps=5,scale=1,acodec=none}:duplicate{dst=std{access=file,mux=mp4,dst='${HOME}/Videos/$(date +%Y-%m-%d)--$(date +%H.%M.%S)_Screencapture.mp4'}}"
}
function screencaptureRight(){
    vlc screen:// --one-instance -I dummy --extraintf oldrc --rc-host localhost:8082 \
    --no-video :screen-fps=5 :screen-caching=300 \
    --screen-left=1920 \
    --sout "#transcode{vcodec=h264,vb=800,fps=5,scale=1,acodec=none}:duplicate{dst=std{access=file,mux=mp4,dst='${HOME}/Videos/$(date +%Y-%m-%d)--$(date +%H.%M.%S)_Screencapture.mp4'}}"
}
function screencaptureLeft(){
    vlc screen:// --one-instance -I dummy --extraintf oldrc --rc-host localhost:8082 \
    --no-video :screen-fps=5 :screen-caching=300 \
    --screen-width=1920 \
    --sout "#transcode{vcodec=h264,vb=800,fps=5,scale=1,acodec=none}:duplicate{dst=std{access=file,mux=mp4,dst='${HOME}/Videos/$(date +%Y-%m-%d)--$(date +%H.%M.%S)_Screencapture.mp4'}}"
}

function screencaptureQuit(){
    echo quit | nc localhost 8082
}
