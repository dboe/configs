"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Daniel Boeckenhoff vimrc configuration
" Installation:
"   1) Symlink this file in your $HOME folder
"
"       ln -s <dir_to_this_file> ~/
"
"   2) Clone vundle to ~/.vim/bundle
"       
"       git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
"
"   3) Open vim and type ':PluginInstall' or call this line via terminal
"
"       vim +PluginInstall +qall
"
"   4) Search for 'Installation:' in this file and follow the instructions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Vundle Start
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible              " be iMproved
filetype off
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

" An idea to put Plugins in envrionments so the Vundle block is spared and
" Settings can be specified after Plugins
" :function PluginEnvironment(plugin_name)
" :  filetype off
" :  call vundle#begin()
" :  Plugin a:plugin_name
" :  call vundle#end()
" :  filetype plugin indent on
" :endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                        Section: General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"UTF-8 support
set encoding=utf-8

" Diesable modelines (lines at the beginning or end of a file which change
" editor behavior)
setlocal nomodeline

" OSX stupid backspace fix
set backspace=indent,eol,start

" Set Proper Tabs
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab

" stop automatic wrapping, unset textwidth
set textwidth=0

" map the leader to space
:let mapleader = " "

" map caps to  escape and revert after leaving vim
au VimEnter * silent! !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
au VimLeave * silent! !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'
:imap jk <Esc>
:vmap jk <Esc>
"
" Write with leader w
nnoremap <silent> <leader>w <C-o>:w<CR>

"""""""""""""""""""""""""""""""""""""
" Appearance
"""""""""""""""""""""""""""""""""""""
" no sound
let g:vim_typing_sound = 0 " sound off
" disable beeping (aka "bell") and no visual bell
set noerrorbells visualbell t_vb=

" Do not wrap code
set nowrap

" Show linenumbers
set number
set relativenumber

" Always display the status line
set laststatus=2

" Enable highlighting of the current line
set cursorline

" Syntax highlighting
syntax on
" Enable 256 colors
set t_Co=256

" Filetype glyphs (icons)
Plugin 'ryanoasis/vim-devicons'
let g:webdevicons_conceal_nerdtree_brackets = 1
let g:WebDevIconsNerdTreeAfterGlyphPadding = ''

" Theme
Plugin 'challenger-deep-theme/vim', {'name': 'challenger-deep-theme'}

" Underline search
let g:spacegray_underline_search = 1
" Italicize comments
let g:spacegray_italicize_comments = 1

" statusline at the bottom of each window
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
let g:airline_powerline_fonts = 1
let g:airline_theme='hybrid'
let g:hybrid_custom_term_colors = 1  " required by hybrid colorscheme
let g:hybrid_reduced_contrast = 1  " required by hybrid colorscheme

"""""""""""""""""""""""""""""""""""""
" Session options
"""""""""""""""""""""""""""""""""""""
set sessionoptions-=blank  " see https://github.com/vim-syntastic/syntastic/issues/1449

"""""""""""""""""""""""""""""""""""""
" Locationlist closing
"""""""""""""""""""""""""""""""""""""
" Autoclose location list at quit
function! QuitOrLclose()
    if &buftype == 'quickfix'
        lclose
    else
        quit
    endif
endfunction

cabbrev q call QuitOrLclose()

"""""""""""""""""""""""""""""""""""""
" gvim specifics
"""""""""""""""""""""""""""""""""""""
" Disable Alt+[menukey] menu keys (i.e. Alt+h for help - this allows
" using custom Alt+something maps)
set winaltkeys=no

" GVIM appearance
:set guioptions-=m  "remove menu bar
:set guioptions-=T  "remove toolbar

"""""""""""""""""""""""""""""""""""""
" nvim specifics
"""""""""""""""""""""""""""""""""""""
" Nvim python environment settings
if has('nvim')
    " Installation: pip install pynvim
    let g:python3_host_prog='/usr/bin/python3'
    " Unicode symbols work fine (nvim, iterm, tmux, nyovim tested)
    let g:pudb_breakpoint_symbol='☠'
endif

"""""""""""""""""""""""""""""""""""""
" spell checking
"""""""""""""""""""""""""""""""""""""
" When to loadwird      # mask  # activate        # language
au BufNewFile,BufRead   *.wiki  setlocal spell    spelllang=en_us
au BufNewFile,BufRead   *.txt   setlocal spell    spelllang=en_us
au BufNewFile,BufRead   *.tex   setlocal spell    spelllang=en_us
au BufNewFile,BufRead   *.md    setlocal spell    spelllang=en_us

" word suggestions over spell checked word
autocmd FileType wiki txt tex md nnoremap <F2> z=
" last and next spell check word
autocmd FileType wiki txt tex md nnoremap <F3> [s
autocmd FileType wiki txt tex md nnoremap <F4> ]s


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Utility
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""
" Git support
"""""""""""""""""""""""""""""""""""""
" If you know how to use Git at the command line, you know how to use :Git.
" It's vaguely akin to :!git but with numerous improvements
Plugin 'tpope/vim-fugitive'
" wrapper around git log --graph
Plugin 'gregsexton/gitv'
" Show a git diff in the gutter
" Nice docu on https://vimawesome.com/plugin/vim-gitgutter
Plugin 'airblade/vim-gitgutter'


"""""""""""""""""""""""""""""""""""""
" Code completion
"""""""""""""""""""""""""""""""""""""
" Use <tab> and <Shift>-<tab> to cycle through proposed completions
Plugin 'ycm-core/YouCompleteMe'
" Automatically closing popups again
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
map <C-n> :NERDTreeToggle<CR>


"""""""""""""""""""""""""""""""""""""
" Snippets/Code Templates
"""""""""""""""""""""""""""""""""""""
" Snippet engine.
" Good documentation here: https://github.com/SirVer/ultisnips/blob/master/doc/UltiSnips.txt
Plugin 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plugin 'honza/vim-snippets'
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
" Use Ctrl+Enter to trigger snippet expansion and Jumping
let g:UltiSnipsExpandTrigger="<C-CR>"
if !exists("g:UltiSnipsJumpForwardTrigger")
    let g:UltiSnipsJumpForwardTrigger = "<C-CR>"
    let g:UltiSnipsJumpBackwardTrigger="<C-S-CR>"
endif
" :UltiSnipsEdit spliting the window
let g:UltiSnipsEditSplit="vertical"

"""""""""""""""""""""""""""""""""""""
" Syntax checkers
"""""""""""""""""""""""""""""""""""""
" Plugin 'vim-syntastic/syntastic'
" " Syntastic recommended Configuration
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*
" 
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" " let g:syntastic_check_on_wq = 0
" let g:syntastic_python_checkers = "['flake8', 'pylint'] pylint is rather slow
" let g:syntastic_python_flake8_exec = 'python3'
" let g:syntastic_python_flake8_args = ['-m', 'flake8']
" 
" let g:syntastic_error_symbol = '❌'
" let g:syntastic_style_error_symbol = '⁉️'
" let g:syntastic_warning_symbol = '⚠️'
" let g:syntastic_style_warning_symbol = '💩'

" Set flake8 and pylint as code checker
" Installation: pip3 install flake8
" Installation: pip3 install pylint
Plugin 'w0rp/ale.git'
let g:ale_linters = {
\   'python': ['flake8', 'pylint'],
\}
" show codecheck errors as a list in a bottom split window
let g:ale_open_list = 1

"""""""""""""""""""""""""""""""""""""
" Building
"""""""""""""""""""""""""""""""""""""
" Asynchronous make (:Neomake)
Plugin 'neomake/neomake'
" General asynchronous processes
Plugin 'skywind3000/asyncrun.vim'

"""""""""""""""""""""""""""""""""""""
" File System
"""""""""""""""""""""""""""""""""""""
""""""""""""""""""""
" Tree File Browser
""""""""""""""""""""
Plugin 'scrooloose/nerdtree'
" close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" change appearance of default arrows
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

""""""""""""""""""""
" Fast file opening with fuzzy find
""""""""""""""""""""
Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plugin 'junegunn/fzf.vim'
" Finding your file by hitting Ctrl-p
nnoremap <C-p> :FZF<CR>
" search for hidden files also
let g:ctrlp_show_hidden = 1
" After finding choose how/where to open by following shortcuts
let g:fzf_action = {
            \ 'ctrl-t': 'tab split',
            \ 'ctrl-s': 'split',
            \ 'ctrl-v': 'vsplit'
            \}
" Advanced customization using autoload functions
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})
" Customize fzf colors to match your color scheme
" - fzf#wrap translates this to a set of --color options
let g:fzf_colors = { 'fg': ['fg', 'Normal'],
            \ 'bg': ['bg', 'Normal'],
            \ 'hl': ['fg', 'Comment'],
            \ 'fg+': ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \ 'bg+': ['bg', 'CursorLine', 'CursorColumn'],
            \ 'hl+': ['fg', 'Statement'],
            \ 'info': ['fg', 'PreProc'],
            \ 'border': ['fg', 'Ignore'],
            \ 'prompt': ['fg', 'Conditional'],
            \ 'pointer': ['fg', 'Exception'],
            \ 'marker': ['fg', 'Keyword'],
            \ 'spinner': ['fg', 'Label'],
            \ 'header': ['fg', 'Comment'] }
" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~40%' }
" In Neovim, you can set up fzf window using a Vim command
let g:fzf_layout = { 'window': 'enew' }
let g:fzf_layout = { 'window': '-tabnew' }
" Enable per-command history
" - History files will be stored in the specified directory
" - When set, CTRL-N and CTRL-P will be bound to 'next-history' and
" 'previous-history' instead of 'down' and 'up'.
if has('win32')
    let g:fzf_history_dir = '$HOME/.local/share/fzf-history'
else
    let g:fzf_history_dir = '~/.local/share/fzf-history'
endif
" Advanced customization using Vim function
inoremap fzf#vim#complete#word({'left': '15%'})
" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1
" TODO ignore node_modules and all files included in the .gitignore file
" maybe with https://github.com/ggreer/the_silver_searcher
" Shortcuts
nnoremap <Leader>o :Files<CR>
nnoremap <Leader>O :CtrlP<CR>
nnoremap <Leader>w :w<CR>

"""""""""""""""""""""""""""""""""""""
" Tagbar
"""""""""""""""""""""""""""""""""""""
" Quick tag browser for the current file, a must have if you are using any
" kind of ctags like exuberant-tags.
Plugin 'majutsushi/tagbar'
" Elixir Tagbar Configuration
let g:tagbar_type_elixir = {
            \ 'ctagstype' : 'elixir',
            \ 'kinds' : [
            \ 'f:functions',
            \ 'functions:functions',
            \ 'c:callbacks',
            \ 'd:delegates',
            \ 'e:exceptions',
            \ 'i:implementations',
            \ 'a:macros',
            \ 'o:operators',
            \ 'm:modules',
            \ 'p:protocols',
            \ 'r:records',
            \ 't:tests'
            \ ]
            \ }

"""""""""""""""""""""""""""""""""""""
" Testing
"""""""""""""""""""""""""""""""""""""
" :TestNearest / :TestFile
Plugin 'janko-m/vim-test'
" Test.vim can run tests using different execution environments ("strategies")
let test#strategy = "neomake"
" mappings:
nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>

"""""""""""""""""""""""""""""""""""""
" Formatting
"""""""""""""""""""""""""""""""""""""
" Format code with formatter on save (e.g. black for python)
" Installation: Requires black to be installed for python: pip3 install black
Plugin 'chiel92/vim-autoformat'
" exectue on writing
" au BufWrite * :Autoformat
" Disable fallback autoformatting
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 0

" line up text with the :Tabularize /<alignment character>
" You do not need to specify a range or visually select thinigs
" For options like alignment look at :help Tabularize
Plugin 'godlygeek/tabular'

" Ctrl+A / Ctrl+X with dates
Plugin 'tpope/vim-speeddating'

" Automatically closing breakets
Plugin 'Townk/vim-autoclose'

" Toggle comments with gcc or gc<movement>
Plugin 'tpope/vim-commentary'

" TODO: Maybe add mustache?
" Plugin 'mustache/vim-mustache-handlebars'
" or
" Plugin 'tobyS/vmustache'

" mappings to easily delete, change and add surroundings (parentheses,
" brackets, quotes, XML tags, and more) in pairs
" E.g. Press ysiw] to add [] around a word
" E.g. Visually select stuff, press S) to add () around a word
Plugin 'tpope/vim-surround'


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: c/c++ specifica
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup CBuild
  autocmd!
  autocmd filetype c   nnoremap <buffer> <leader>cc :!gcc -o %:p:r %<cr>
  autocmd filetype c   nnoremap <buffer> <leader>cr :!gcc -o %:p:r %<cr>:!%:p:r<cr>
  autocmd filetype cpp nnoremap <buffer> <leader>cc :!g++ -o %:p:r %<cr>
  autocmd filetype cpp nnoremap <buffer> <leader>cr :!g++ -o %:p:r %<cr>:!%:p:r<cr>
augroup END


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: python specifica
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" tabs, spaces, linebreaks
au BufNewFile,BufRead *.py setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=100 expandtab autoindent fileformat=unix

" converts Vim into a Python IDE
" Support Python and 3.6+
" Syntax highlighting
" Virtualenv support
" Run python code (<leader>r)
" Add/remove breakpoints (<leader>b)
" Improved Python indentation
" Python motions and operators (]], 3[[, ]]M, vaC, viM, daC, ciM, ...)
" Improved Python folding
" Run multiple code checkers simultaneously (:PymodeLint)
" Autofix PEP8 errors (:PymodeLintAuto)
" Search in python documentation (<leader>K)
" Code refactoring
" Intellisense code-completion
" Go to definition (<C-c>g)
Plugin 'python-mode/python-mode'
let g:pymode_python = 'python3'
let g:pymode_rope = 0
let g:pymode_rope_completion = 0
let g:pymode_lint = 0
let g:pymode_doc = 0
"let g:pymode_folding = 0
"let g:pymode_virtualenv = 0
"let g:pymode_syntax = 0
let g:pymode_indent = 1
let g:pymode_options_max_line_length = 99
" autoindent will help, but in some cases (like when a function signature spans
" multiple lines), it doesn’t always do what you want, especially when it comes
" to conforming to PEP 8 standards. To fix that, you can use the
" indentpython.vim extension
Plugin 'vim-scripts/indentpython.vim'

" Indent Python in the Google way (linked from http://google.github.io/styleguide/pyguide.html).
setlocal indentexpr=GetGooglePythonIndent(v:lnum)
let s:maxoff = 50 " maximum number of lines to look backwards.
function GetGooglePythonIndent(lnum)
    " Indent inside parens.
    " Align with the open paren unless it is at the end of the line.
    " E.g.
    "   open_paren_not_at_EOL(100,
    "                         (200,
    "                          300),
    "                         400)
    "   open_paren_at_EOL(
    "       100, 200, 300, 400)
    call cursor(a:lnum, 1)
    let [par_line, par_col] = searchpairpos('(\|{\|\[', '', ')\|}\|\]', 'bW',
                \ "line('.') < " . (a:lnum - s:maxoff) . " ? dummy :"
                \ . " synIDattr(synID(line('.'), col('.'), 1), 'name')"
                \ . " =~ '\\(Comment\\|String\\)$'")
    if par_line > 0
        call cursor(par_line, 1)
        if par_col != col("$") - 1
            return par_col
        endif
    endif
    " Delegate the rest to the original function.
    return GetPythonIndent(a:lnum)
endfunction
let pyindent_nested_paren="&sw*2"
let pyindent_open_paren="&sw*2"

"""""""""""""""""""""""""""""""""""""
" Jupyter like cells and ipython in terminal support
"""""""""""""""""""""""""""""""""""""
Plugin 'jpalardy/vim-slime'
Plugin 'hanschen/vim-ipython-cell'
if has('autocmd')
    filetype plugin indent on
endif
""""""""""""""""""""
" slime configuration
""""""""""""""""""""
" always use screen
let g:slime_target = 'screen'

" The function in this script creates a list of [<buffer name>, <window number>] entries by applying map() to the range of window numbers in the current tabpage.
" winnr('$') returns the number of windows in the tabpage. Then it finds the entry with the given buffer name, and extracts the associated window number.
" Then it jumps to that window.
function! s:win_by_bufname(bufname)
    let bufmap = map(range(1, winnr('$')), '[bufname(winbufnr(v:val)), v:val]')
    let thewindow = filter(bufmap, 'v:val[0] =~ a:bufname')[0][1]
    execute thewindow 'wincmd w'
endfunction

command! -nargs=* WinGo call s:win_by_bufname(<q-args>)

" open new window, deactivate conda (bug in conda), start a screen with embedded ipython
let g:ipython=0
" Works but kills mother terminal
" nnoremap <expr> <F9> IPythonToggle() ? ':vertical terminal ++close bash<CR>conda deactivate<CR>screen -S vim-ipython-session -t vim-ipython-window<CR>ipython<CR><C-w><C-w>:wincmd g:winid<CR>':':WinGo bash<CR>!kill -9 `pgrep -f bash`<CR>'

"File content of vimQuitIpython in home directory (replace <apost> by double quote
"    #!/bin/bash
"    screen_pid=$(screen -ls | awk '/\.vim-ipython-session\t/ {print strtonum($1)}' | head -n 1)
"    vim_rank=$(pstree -laps $screen_pid | grep -n -m 1 vim | sed  's/\([0-9]*\).*/\1/')
"    bash_rank=$(($vim_rank+1))
"    bash_pid=$(pstree -laps $screen_pid | sed <apost>${bash_rank}q;d<apost> | cut -d <apost>,<apost> -f2)
"    kill -n 9 $bash_pid
"    screen -X -S vim-ipython-session quit
function! QuitIpython()
    !bash ~/.vim/vimQuitIpython
endfunction
autocmd FileType python nnoremap <expr> <Leader>i IPythonToggle() ? ':vertical terminal ++close bash<CR>conda deactivate<CR>screen -S vim-ipython-session -t vim-ipython-window<CR>ipython<CR><C-w><C-w>:wincmd g:winid<CR>':':call QuitIpython()<CR>'
" screen_pid=$(screen -ls | awk '/\.vim-ipython-session\t/ {print strtonum($1)}' | head -n 1) && vim_rank=$(pstree -laps $screen_pid | grep -n -m 1 vim | sed  `s/\([0-9]*\).*/\1/`) && bash_rank=$(($vim_rank+1)) && bash_pid=$(pstree -laps $screen_pid | sed `${bash_rank}q;d` | cut -d `,` -f2) && kill -n 9 $bash_pid
function! IPythonToggle()
    let g:winid=win_getid()
    if g:ipython
        let g:ipython=0
    else
        let g:ipython=1
    endif
    return g:ipython
endfunction
" fix paste issues in ipython
let g:slime_python_ipython = 1
let g:slime_default_config = {"sessionname": "vim-ipython-session", "windowname": "vim-ipython-window"}
let g:slime_dont_ask_default = 1
""""""""""""""""""""
" ipython-cell configuration
""""""""""""""""""""
" Use '##' to define cells instead of using marks
let g:ipython_cell_delimit_cells_by = 'tags'
" map <Leader>r to run script
autocmd FileType python nnoremap <buffer> <Leader>r :IPythonCellRun<CR>
" map <Leader>R to run script and time the execution
autocmd FileType python nnoremap <buffer> <Leader>R :IPythonCellRunTime<CR>
" map <Leader>c to execute the current cell
autocmd FileType python nnoremap <buffer> <Leader>c :IPythonCellExecuteCell<CR>
" map <Leader>C to execute the current cell and jump to the next cell
autocmd FileType python nnoremap <buffer> <Leader>C :IPythonCellExecuteCellJump<CR>
autocmd FileType python nnoremap <buffer> <S-CR> :IPythonCellExecuteCellJump<CR>
" map <Leader>l to clear IPython screen
autocmd FileType python nnoremap <buffer> <Leader>l :IPythonCellClear<CR>
"nnoremap <A-l> :IPythonCellClear<CR>
" map <Leader>x to close all Matplotlib figure windows
autocmd FileType python nnoremap <buffer> <Leader>x :IPythonCellClose<CR>
" map [c and ]c to jump to the previous and next cell header
autocmd FileType python nnoremap <buffer> [c :IPythonCellPrevCell<CR>
autocmd FileType python nnoremap <buffer> ]c :IPythonCellNextCell<CR>
" map <Leader>h to send the current line or current selection to IPython
autocmd FileType python nnoremap <buffer> <Leader>h <Plug>SlimeLineSend
autocmd FileType python xnoremap <buffer> <Leader>h <Plug>SlimeRegionSend

"""""""""""""""""""""""""""""""""""""
" Debugging
"""""""""""""""""""""""""""""""""""""

" " Plugin 'SkyLeach/pudb.vim'
" " Installation:
" "     * Follow this https://puremourning.github.io/vimspector-web/demo-setup.html
" "     * The addition to the vimrc is obviously done already
" let g:vimspector_enable_mappings = 'HUMAN'
" packadd! vimspector

" Debugging
" https://github.com/serebrov/vimrc/blob/b8c2349ebb1eedd294ab0b65ecc4367e3a4df3c7/.vimrc#L1543
" The above can be a source of inspiration for debug dialects
Plugin 'vim-vdebug/vdebug', {'on': 'VdebugStart'}
" Installation: pip3 install komodo-python3-dbgp
" TODO: install patch (see python section of :help vdebug)
" Do not stop on the first line, Stop on the first breakpoint
" Stop listening immediately after a debugging session has finished
let g:vdebug_keymap = {
            \    'run' : '',
            \    'run_to_cursor' : '<F9>',
            \    'step_over' : '<F2>',
            \    'step_into' : '<F3>',
            \    'step_out' : '<F4>',
            \    'close' : '',
            \    'detach' : '<F7>',
            \    'set_breakpoint' : '<F10>',
            \    'get_context' : '<F11>',
            \    'eval_under_cursor' : '<F12>',
            \    'eval_visual' : '<Leader>e'
            \}
" Start the server and start debugging
" TODO: loop the debugger / destroy the process.
nnoremap <F5> :VdebugStart<CR>:AsyncRun pydbgp %:p /<CR>
nnoremap <F6> :AsyncStop <CR>:python3 debugger.close()<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: LaTeX specifica
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" tabs, spaces, linebreaks
au BufNewFile,BufRead *.tex setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=0 expandtab autoindent fileformat=unix wrap linebreak nolist

" live Preview
Plugin 'vim-latex-live-preview'
let g:livepreview_previewer = 'okular'
autocmd FileType tex,latex nnoremap <buffer> <F6> :LLPStartPreview<CR>

" vim-latexsuite
" F5 -> environment manager
" F9 in between \ref{} / \cite{} / includegraphics brackets -> list of options
"
" Normal-Mode-Shortcuts: 
" `^   expands to   \Hat{<++>}<++>
" `_   expands to   \bar{<++>}<++>
" `6   expands to   \partial
" `8   expands to   \infty
" `/   expands to   \frac{<++>}{<++>}<++>
" `%   expands to   \frac{<++>}{<++>}<++>
" `@   expands to   \circ
" `0   expands to   ^\circ
" `=   expands to   \equiv
" `\   expands to   \setminus
" `.   expands to   \cdot
" `*   expands to   \times
" `&   expands to   \wedge
" `-   expands to   \bigcap
" `+   expands to   \bigcup
" `(   expands to   \subset
" `)   expands to   \supset
" `<   expands to   \le
" `>   expands to   \ge
" `,   expands to   \nonumber
" `~   expands to   \tilde{<++>}<++>
" `;   expands to   \dot{<++>}<++>
" `:   expands to   \ddot{<++>}<++>
" `2   expands to   \sqrt{<++>}<++>
" `|   expands to   \Big|
" `I   expands to   \int_{<++>}^{<++>}<++>
"
" Visual-Mode-Shortcuts: 
" `(  encloses selection in \left( and \right)
" `[  encloses selection in \left[ and \right]
" `{  encloses selection in \left\{ and \right\}
" `$  encloses selection in $$ or \[ \] depending on characterwise or linewise selection
Plugin 'vim-latex/vim-latex'
" environment shortcuts:
let g:Tex_Env_itemize = "\\begin{itemize}\<CR>\\item <++>\<CR>\\end{itemize}\<CR><++>"
let g:Tex_Env_description = "\\begin{description}\<CR>\\item[<++>] <++>\<CR>\\end{description}\<CR><++>"
let g:Tex_PromptedEnvironments='equation,equation*,align,align*,itemize,enumerate,description,figure'

" changes package accept
function! AcceptChanges()
    " Hover over '\replaced', '\added' or '\deleted' from the changes package and
    " remove it
    "E.g. \replaced{New}{Old} -> New
    "E.g. \replaced[comment=asdf]{New}{Old} -> New
    "E.g. \added{New} -> New
    "E.g. \deleted[id=3]{Old} -> 
    "E.g. Conti\replaced[comment=asdf]{nuing}{uing} -> Continuing
    "E.g. \replaced[comment={asdf}]{New}{Old} -> New
    "E.g. \replaced[comment={asdf my strange [] {}}]{New}{Old} -> New
    "E.g. \replaced[%
    "       comment={%
    "           asdf my strange [] {}
    "           }
    "     ]{%
    "       New
    "     }{%
    "     Old
    "     } -> New
    "
    "
    "TODO:
    let word = expand('<cword>')
    let WORD = expand('<cWORD>')
    if (word == 'replaced') || (word == 'added') || (word == 'deleted') || (word == 'highlight') || (word == 'comment') || (word == 'TODO')
        " " go one char right to be sure to catch the \
        " normal l
        " " search backwards to \
        " normal ?\\\\
        " delete till {}
        if stridx(WORD, '[') >= 0 && ((stridx(WORD, '[') < stridx(WORD, '{')) || stridx(WORD, '{') < 0)
            echo "[ IN THERE ]"
            normal dt[
            normal d%
            normal iX
        endif
        normal dt{
        if (word == 'replaced') || (word == 'added') || (word == 'highlight') || (word == 'comment')
            " Save cursor position
            let l:save_cursor = getpos(".") 
            " Go to matching bracket
            normal %
            " Delete bracket
            normal dl
            if (word == 'replaced')
                " Delete Old part
                normal d%
            endif
            " Restore cursor position
            call setpos('.', l:save_cursor)
            " Delete bracket before New
            normal dl
        else 
            " Delete Old part
            normal d%
        endif
    endif
endfunction

autocmd FileType tex,latex nnoremap <Leader>y :call AcceptChanges()<CR>

autocmd FileType tex,latex noremap <Leader>n /\\replaced\\|\\added\\|\\deleted<CR>
autocmd FileType tex,latex noremap <Leader>N ?\\replaced\\|\\added\\|\\deleted<CR>
autocmd FileType tex,latex noremap <Leader>a i\added[id=DB,comment=<++>]{<++>}
autocmd FileType tex,latex inoremap <C-a> \added[id=DB,comment=<++>]{<++>}
autocmd FileType tex,latex noremap <Leader>t i\TODO{DB: <++>}
autocmd FileType tex,latex xmap <Leader>d S}i\deleted[id=DB,comment=<++>]
autocmd FileType tex,latex xmap <Leader>h S}i\highlight[id=DB,comment=<++>]
autocmd FileType tex,latex xmap <Leader>c S}i\comment[id=DB]
autocmd FileType tex,latex xmap <Leader>r S}i\replaced[id=DB,comment=<++>]{<++>}


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Markdown/Text specifica
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup markdown
    au!
    au BufNewFile,BufRead *.md,*.markdown setlocal filetype=ghmarkdown
augroup END

" Syntax highlighting, matching rules and mappings for the original Markdown
" and extensions.
Plugin 'plasticboy/vim-markdown'
" recognize github code blocks
Plugin 'jtratner/vim-flavored-markdown'
" Grammar checker
Plugin 'dpelle/vim-LanguageTool'
" Install: See "Download Language Tool " at https://github.com/dpelle/vim-LanguageTool
"     You need to put the path to the commandline.java file here
:let g:languagetool_jar='$HOME/.vim/LanguageTool-4.9/languagetool-commandline.jar'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Motion
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tab navigation like in Firefox or the Terminal.
nnoremap <C-S-tab> :tabprevious<CR>
inoremap <C-S-tab> <Esc>:tabprevious<CR>i
nnoremap <C-tab>   :tabnext<CR>
inoremap <C-tab>   <Esc>:tabnext<CR>i
nnoremap <C-t>     :tabnew<CR>
inoremap <C-t>     <Esc>:tabnew<CR>
nnoremap <A-1> 1gt
nnoremap <A-2> 2gt
nnoremap <A-3> 3gt
nnoremap <A-4> 4gt
nnoremap <A-5> 5gt
nnoremap <A-6> 6gt
nnoremap <A-7> 7gt
nnoremap <A-8> 8gt
nnoremap <A-9> 9gt
nnoremap <A-0> 10gt

" use alt+hjkl to move between split/vsplit panels
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" Disable arrow movement, resize splits instead.
nnoremap <Up>    :resize +2<CR>
nnoremap <Down>  :resize -2<CR>
nnoremap <Left>  :vertical resize +2<CR>
nnoremap <Right> :vertical resize -2<CR>


" Eeasy motion by searching occurence of characters. Similar plugin is
" vim-sneak
Plugin 'easymotion/vim-easymotion'
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)
" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)
" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)
" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Section: Vundle End
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on


" choice of the color scheme only possible after vundle end
colorscheme challenger_deep
let g:challenger_deep_termcolors = 256
