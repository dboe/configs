// Mozilla User Preferences inherited by prefs.js
// You could think of making this dynamic for every user by importing another file with the dynamic (per-user) settings.

user_pref("extensions.zotero.recursiveCollections", true);
user_pref("extensions.zotero.sync.fulltext.enabled", false);
user_pref("extensions.zotero.sync.server.username", "dboeckenhoff");
user_pref("extensions.zotero.sync.storage.groups.enabled", false);
user_pref("extensions.zotero.sync.storage.protocol", "webdav");
user_pref("extensions.zotero.sync.storage.url", "datashare.mpcdf.mpg.de/remote.php/dav/files/dboe/Subjects/IPP/Documents/Literature/zotero_web_dav");
user_pref("extensions.zotero.sync.storage.username", "dboe");
user_pref("extensions.zotero.translators.better-bibtex.DOIandURL", "both");
user_pref("extensions.zotero.translators.better-bibtex.ascii", "");
user_pref("extensions.zotero.translators.better-bibtex.asciiBibLaTeX", false);
user_pref("extensions.zotero.translators.better-bibtex.asciiBibTeX", true);
user_pref("extensions.zotero.translators.better-bibtex.autoAbbrev", false);
user_pref("extensions.zotero.translators.better-bibtex.autoAbbrevStyle", "");
user_pref("extensions.zotero.translators.better-bibtex.autoExport", "immediate");
user_pref("extensions.zotero.translators.better-bibtex.autoExportDelay", 5);
user_pref("extensions.zotero.translators.better-bibtex.autoExportIdleWait", 10);
user_pref("extensions.zotero.translators.better-bibtex.autoExportPathReplaceDiacritics", false);
user_pref("extensions.zotero.translators.better-bibtex.autoExportPathReplaceDirSep", "-");
user_pref("extensions.zotero.translators.better-bibtex.autoExportPathReplaceSpace", " ");
user_pref("extensions.zotero.translators.better-bibtex.autoPinDelay", 0);
user_pref("extensions.zotero.translators.better-bibtex.automaticTags", true);
user_pref("extensions.zotero.translators.better-bibtex.auxImport", false);
user_pref("extensions.zotero.translators.better-bibtex.baseAttachmentPath", "");
user_pref("extensions.zotero.translators.better-bibtex.biblatexExtendedDateFormat", true);
user_pref("extensions.zotero.translators.better-bibtex.biblatexExtendedNameFormat", false);
user_pref("extensions.zotero.translators.better-bibtex.biblatexExtractEprint", true);
user_pref("extensions.zotero.translators.better-bibtex.bibtexParticleNoOp", false);
user_pref("extensions.zotero.translators.better-bibtex.bibtexURL", "off");
user_pref("extensions.zotero.translators.better-bibtex.cacheFlushInterval", 5);
user_pref("extensions.zotero.translators.better-bibtex.caching", true);
user_pref("extensions.zotero.translators.better-bibtex.citeCommand", "cite");
user_pref("extensions.zotero.translators.better-bibtex.citekeyFold", true);
user_pref("extensions.zotero.translators.better-bibtex.citekeyFormat", "[auth:capitalize][year]");
user_pref("extensions.zotero.translators.better-bibtex.citekeySearch", true);
user_pref("extensions.zotero.translators.better-bibtex.citeprocNoteCitekey", false);
user_pref("extensions.zotero.translators.better-bibtex.csquotes", "");
user_pref("extensions.zotero.translators.better-bibtex.debugLogDir", "");
user_pref("extensions.zotero.translators.better-bibtex.exportBibTeXStrings", "off");
user_pref("extensions.zotero.translators.better-bibtex.exportBraceProtection", true);
user_pref("extensions.zotero.translators.better-bibtex.exportTitleCase", true);
user_pref("extensions.zotero.translators.better-bibtex.extraMergeCSL", false);
user_pref("extensions.zotero.translators.better-bibtex.extraMergeCitekeys", false);
user_pref("extensions.zotero.translators.better-bibtex.extraMergeTeX", false);
user_pref("extensions.zotero.translators.better-bibtex.git", "config");
user_pref("extensions.zotero.translators.better-bibtex.ignorePostscriptErrors", true);
user_pref("extensions.zotero.translators.better-bibtex.import", true);
user_pref("extensions.zotero.translators.better-bibtex.importBibTeXStrings", true);
user_pref("extensions.zotero.translators.better-bibtex.importCaseProtection", "as-needed");
user_pref("extensions.zotero.translators.better-bibtex.importCitationKey", true);
user_pref("extensions.zotero.translators.better-bibtex.importExtra", true);
user_pref("extensions.zotero.translators.better-bibtex.importJabRefAbbreviations", true);
user_pref("extensions.zotero.translators.better-bibtex.importJabRefStrings", true);
user_pref("extensions.zotero.translators.better-bibtex.importSentenceCase", "on+guess");
user_pref("extensions.zotero.translators.better-bibtex.importUnknownTexCommand", "ignore");
user_pref("extensions.zotero.translators.better-bibtex.itemObserverDelay", 5);
user_pref("extensions.zotero.translators.better-bibtex.jabrefFormat", 0);
user_pref("extensions.zotero.translators.better-bibtex.jieba", false);
user_pref("extensions.zotero.translators.better-bibtex.keyConflictPolicy", "keep");
user_pref("extensions.zotero.translators.better-bibtex.keyScope", "library");
user_pref("extensions.zotero.translators.better-bibtex.kuroshiro", false);
user_pref("extensions.zotero.translators.better-bibtex.mapMath", "");
user_pref("extensions.zotero.translators.better-bibtex.mapText", "");
user_pref("extensions.zotero.translators.better-bibtex.mapUnicode", "conservative");
user_pref("extensions.zotero.translators.better-bibtex.newTranslatorsAskRestart", true);
user_pref("extensions.zotero.translators.better-bibtex.parseParticles", true);
user_pref("extensions.zotero.translators.better-bibtex.patchDates", "");
user_pref("extensions.zotero.translators.better-bibtex.platform", "lin");
user_pref("extensions.zotero.translators.better-bibtex.postscript", "");
user_pref("extensions.zotero.translators.better-bibtex.postscriptOverride", "");
user_pref("extensions.zotero.translators.better-bibtex.preferencesOverride", "");
user_pref("extensions.zotero.translators.better-bibtex.qualityReport", false);
user_pref("extensions.zotero.translators.better-bibtex.quickCopyEta", "");
user_pref("extensions.zotero.translators.better-bibtex.quickCopyMode", "latex");
user_pref("extensions.zotero.translators.better-bibtex.quickCopyOrgMode", "zotero");
user_pref("extensions.zotero.translators.better-bibtex.quickCopyPandocBrackets", false);
user_pref("extensions.zotero.translators.better-bibtex.quickCopySelectLink", "zotero");
user_pref("extensions.zotero.translators.better-bibtex.rawImports", false);
user_pref("extensions.zotero.translators.better-bibtex.rawLaTag", "#LaTeX");
user_pref("extensions.zotero.translators.better-bibtex.relativeFilePaths", false);
user_pref("extensions.zotero.translators.better-bibtex.retainCache", false);
user_pref("extensions.zotero.translators.better-bibtex.scrubDatabase", false);
user_pref("extensions.zotero.translators.better-bibtex.separatorList", "and");
user_pref("extensions.zotero.translators.better-bibtex.separatorNames", "and");
user_pref("extensions.zotero.translators.better-bibtex.skipFields", "");
user_pref("extensions.zotero.translators.better-bibtex.skipWords", "a,ab,aboard,about,above,across,after,against,al,along,amid,among,an,and,anti,around,as,at,before,behind,below,beneath,beside,besides,between,beyond,but,by,d,da,das,de,del,dell,dello,dei,degli,della,dell,delle,dem,den,der,des,despite,die,do,down,du,during,ein,eine,einem,einen,einer,eines,el,en,et,except,for,from,gli,i,il,in,inside,into,is,l,la,las,le,les,like,lo,los,near,nor,of,off,on,onto,or,over,past,per,plus,round,save,since,so,some,sur,than,the,through,to,toward,towards,un,una,unas,under,underneath,une,unlike,uno,unos,until,up,upon,versus,via,von,while,with,within,without,yet,zu,zum");
user_pref("extensions.zotero.translators.better-bibtex.startupProgress", "popup");
user_pref("extensions.zotero.translators.better-bibtex.strings", "");
user_pref("extensions.zotero.translators.better-bibtex.stringsOverride", "");
user_pref("extensions.zotero.translators.better-bibtex.testing", false);
user_pref("extensions.zotero.translators.better-bibtex.verbatimFields", "url,doi,file,eprint,verba,verbb,verbc,groups");
user_pref("extensions.zotero.translators.better-bibtex.warnBulkModify", 10);
user_pref("extensions.zotero.translators.better-bibtex.warnTitleCased", false);
user_pref("extensions.zotero.translators.better-bibtex.workers", 1);
user_pref("extensions.zotero.useDataDir", true);
